import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-divinia',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './divina.component.html',
  styleUrl: './divina.component.css'
})
export class DivinaComponent implements  AfterViewInit {

  @ViewChild('videoRef') videoElement!: ElementRef<HTMLVideoElement>;

  ngAfterViewInit(): void {
    const video = this.videoElement.nativeElement;

    video.addEventListener('loadedmetadata', () => {
      video.currentTime = 1;
    });
  }




}
