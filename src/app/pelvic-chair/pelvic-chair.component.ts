import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-pelvic-chair',
  standalone: true,
  imports: [],
  templateUrl: './pelvic-chair.component.html',
  styleUrl: './pelvic-chair.component.css'
})
export class PelvicChairComponent implements OnInit, AfterViewInit {

  constructor(private renderer: Renderer2) {

  }

  @ViewChild('pelvic') videoElement!: ElementRef<HTMLVideoElement>;

  ngAfterViewInit(): void {
    const video = this.videoElement.nativeElement;

    video.addEventListener('loadedmetadata', () => {
      video.currentTime = 1;
    });
  }

  ngOnInit(): void {
    const script = this.renderer.createElement('script');
    script.src = 'https://assets.calendly.com/assets/external/widget.js';
    script.type = 'text/javascript';
    script.async = true;
    this.renderer.appendChild(document.body, script);
  }

  scrollToSection(): void {
    const section = document.getElementById('termin-pelvic');
    if (section) {
      section.scrollIntoView({behavior: 'smooth', block: 'start'});
    }
  }

}
