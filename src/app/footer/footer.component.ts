import {Component} from '@angular/core';
import {RouterLink} from "@angular/router";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {faEnvelope, faGlobe} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [
    RouterLink,
    FontAwesomeModule
  ],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.css'
})
export class FooterComponent {
  faEnvelope = faEnvelope;
  faGlobe = faGlobe;

  email: string = "tanja.dallinger@luckycurves.at";
}
