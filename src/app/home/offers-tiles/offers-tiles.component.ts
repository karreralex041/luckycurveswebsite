import {Component} from '@angular/core';
import {NgOptimizedImage} from "@angular/common";
import {Router} from "@angular/router";

@Component({
  selector: 'app-offers-tiles',
  standalone: true,
  imports: [
    NgOptimizedImage,
  ],
  templateUrl: './offers-tiles.component.html',
  styleUrl: './offers-tiles.component.css'
})
export class OffersTilesComponent {
  constructor(
    private router: Router
  ) {
  }

  routeTo(url: string) {
    this.router.navigate([url]).then(() => {
      window.scrollTo(0, 0);
    });
  }
}
