import {Component} from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-spinning-banner',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './spinning-banner.component.html',
  styleUrl: './spinning-banner.component.css'
})
export class SpinningBannerComponent {

}
