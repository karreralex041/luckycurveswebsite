import {Component, OnInit, Renderer2} from '@angular/core';
import {OffersTilesComponent} from "./offers-tiles/offers-tiles.component";
import {ContactComponent} from "../contact/contact.component";
import {SpinningBannerComponent} from "./spinning-banner/spinning-banner.component";
import {ActivatedRoute, Router} from "@angular/router";
import emailjs from "@emailjs/browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

declare var bootstrap: any;

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    OffersTilesComponent,
    ContactComponent,
    SpinningBannerComponent,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  constructor(private router: Router,
              private route: ActivatedRoute,
              private renderer: Renderer2) {
  }


  name: string = '';
  message: string = '';
  emailForMail: string = '';
  phone: string = '';
  protected errorMessage: string = '';
  protected successMessage: string = '';
  private actionModal: any;
  private contactModal: any;

  ngOnInit() {
    const modalElement = document.getElementById('aktionModal');
    const actionElement = document.getElementById('contactModal');
    this.contactModal = new bootstrap.Modal(actionElement);
    this.actionModal = new bootstrap.Modal(modalElement);

    const lastShown = localStorage.getItem('modalLastShown');
    const now = new Date().getTime();
    const sevenDays = 2 * 24 * 60 * 60 * 1000;

    if (!lastShown || now - parseInt(lastShown, 10) > sevenDays) {
      this.openDialog();
      localStorage.setItem('modalLastShown', now.toString());
    }
    const script = this.renderer.createElement('script');
    script.src = 'https://assets.calendly.com/assets/external/widget.js';
    script.type = 'text/javascript';
    script.async = true;
    this.renderer.appendChild(document.body, script);

    this.route.fragment.subscribe((fragment) => {
      if (fragment) {
        this.scrollToSection(fragment);
      }
    });
  }


  openDialog() {
    this.actionModal.show();
  }

  openContact() {
    this.closeDialog();
    this.contactModal.show();
  }

  closeContact() {
    this.contactModal.hide();
  }

  closeDialog() {
    this.actionModal.hide();
  }

  sendEmail() {
    this.errorMessage = '';
    this.successMessage = '';

    if (!this.name || this.name.trim() === '') {
      this.errorMessage = 'Bitte gib deinen Namen ein.';
      return;
    }

    if (!this.emailForMail || this.emailForMail.trim() === '') {
      this.errorMessage = 'Bitte gib deine E-Mail-Adresse ein.';
      return;
    } else if (!this.validateEmail(this.emailForMail)) {
      this.errorMessage = 'Bitte gib eine gültige E-Mail-Adresse ein.';
      return;
    }

    if (!this.phone || this.phone.trim() === '') {
      this.errorMessage = 'Bitte gib deine Telefonnummer ein.';
      return;
    } else if (!this.validatePhone(this.phone)) {
      this.errorMessage = 'Bitte gib eine gültige Telefonnummer ein.';
      return;
    }

    if (!this.message || this.message.trim() === '') {
      this.errorMessage = 'Bitte gib eine Nachricht ein.';
      return;
    }

    const templateParams = {
      name: this.name,
      reply_to: this.emailForMail,
      phone: this.phone,
      message: this.message
    };

    emailjs.send('service_rola9ds', 'template_bgmczpk', templateParams, 'koZpkYhY_LDFlJnna')
      .then((response) => {
        console.log('E-Mail erfolgreich gesendet!', response.status, response.text);
        this.closeContact();
        this.successMessage = 'Deine Nachricht wurde erfolgreich versendet!';
        this.name = '';
        this.emailForMail = '';
        this.phone = '';
        this.message = '';
      }, (error) => {
        console.error('Fehler beim Senden der E-Mail:', error);
        this.errorMessage = 'Es gab ein Problem beim Versenden der E-Mail. Bitte versuche es später erneut.';
      });
  }

  validateEmail(email: string): boolean {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  validatePhone(phone: string): boolean {
    const phoneRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/;
    return phoneRegex.test(phone);
  }

  scrollToSection(fragment: string) {
    const element = document.getElementById(fragment);
    if (element) {
      element.scrollIntoView({behavior: 'smooth', block: 'start'});
    } else {
      console.warn(`Element mit ID "${fragment}" nicht gefunden.`);
    }
  }
}
