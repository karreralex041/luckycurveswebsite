import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {FaIconComponent} from "@fortawesome/angular-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-group-course',
  standalone: true,
  imports: [
    FaIconComponent,
    RouterLink
  ],
  templateUrl: './group-course.component.html',
  styleUrl: './group-course.component.css'
})
export class GroupCourseComponent implements AfterViewInit {

  @ViewChild('gruppenKurs') videoElement!: ElementRef<HTMLVideoElement>;
  protected readonly faClock = faClock;

  ngAfterViewInit(): void {
    const video = this.videoElement.nativeElement;

    video.addEventListener('loadedmetadata', () => {
      video.currentTime = 2;
    });
  }
}
