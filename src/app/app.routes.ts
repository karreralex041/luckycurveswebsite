import {Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {DataComponent} from "./data/data.component";
import {ImprintComponent} from "./imprint/imprint.component";
import {PowerPlateComponent} from "./power-plate/power-plate.component";
import {GroupCourseComponent} from "./group-course/group-course.component";
import {DivinaComponent} from "./divinia/divina.component";
import {PelvicChairComponent} from "./pelvic-chair/pelvic-chair.component";

export const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'data-policy', component: DataComponent},
  {path: 'imprint', component: ImprintComponent},
  {path: 'power-plate', component: PowerPlateComponent},
  {path: 'group-course', component: GroupCourseComponent},
  {path: 'divinia', component: DivinaComponent},
  {path: 'pelvic-chair', component: PelvicChairComponent},
  {path: '**', redirectTo: '/home',}
];
