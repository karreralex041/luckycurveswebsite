import {Component} from '@angular/core';
import {NgIf} from "@angular/common";
import {faEnvelope, faGlobe, faPhone} from "@fortawesome/free-solid-svg-icons";
import {FaIconComponent} from "@fortawesome/angular-fontawesome";
import emailjs from '@emailjs/browser';
import {FormsModule} from "@angular/forms";
import {catchError, forkJoin, from, tap} from "rxjs";

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [
    FaIconComponent,
    NgIf,
    FormsModule
  ],
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  email: string = "tanja.dallinger@luckycurves.at";
  name: string = '';
  emailForMail: string = '';
  phone: string = '';
  message: string = '';
  protected readonly faGlobe = faGlobe;
  protected readonly faEnvelope = faEnvelope;
  protected readonly faPhone = faPhone;
  protected errorMessage: string = '';
  protected successMessage: string = '';

  sendEmail() {
    this.errorMessage = '';
    this.successMessage = '';

    if (!this.name || this.name.trim() === '') {
      this.errorMessage = 'Bitte gib deinen Namen ein.';
      return;
    }

    if (!this.emailForMail || this.emailForMail.trim() === '') {
      this.errorMessage = 'Bitte gib deine E-Mail-Adresse ein.';
      return;
    } else if (!this.validateEmail(this.emailForMail)) {
      this.errorMessage = 'Bitte gib eine gültige E-Mail-Adresse ein.';
      return;
    }

    if (!this.phone || this.phone.trim() === '') {
      this.errorMessage = 'Bitte gib deine Telefonnummer ein.';
      return;
    } else if (!this.validatePhone(this.phone)) {
      this.errorMessage = 'Bitte gib eine gültige Telefonnummer ein.';
      return;
    }

    if (!this.message || this.message.trim() === '') {
      this.errorMessage = 'Bitte gib eine Nachricht ein.';
      return;
    }

    const templateParams = {
      name: this.name,
      reply_to: this.emailForMail,
      phone: this.phone,
      message: this.message
    };


    const params = {
      name: this.name,
      mail: this.emailForMail
    }

    forkJoin([
      from(emailjs.send("service_1tbirzh", "template_lzxnv8l", params, '_XdTI6ysi5ECCszKM')),
      from(emailjs.send("service_1tbirzh", "template_uyjpz3j", templateParams, '_XdTI6ysi5ECCszKM'))
    ]).pipe(
      tap( ()=> {
        this.successMessage = 'Deine Nachricht wurde erfolgreich versendet.';
        this.name = '';
        this.emailForMail = '';
        this.phone = '';
        this.message = '';
      }, catchError((error) => {
        console.error('Fehler beim Senden der E-Mails', error);
        this.errorMessage = 'Beim Senden der E-Mails ist ein Fehler aufgetreten.';
        return error;
      }))
    ).subscribe();

  }

  validateEmail(email: string): boolean {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  validatePhone(phone: string): boolean {
    const phoneRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/;
    return phoneRegex.test(phone);
  }
}
