import {Component} from '@angular/core';
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {FaIconComponent} from "@fortawesome/angular-fontawesome";

@Component({
  selector: 'app-pricing',
  standalone: true,
  imports: [
    FaIconComponent
  ],
  templateUrl: './pricing.component.html',
  styleUrl: './pricing.component.css'
})
export class PricingComponent {

  protected readonly faClock = faClock;
}
